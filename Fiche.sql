-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 17 Mai 2019 à 16:51
-- Version du serveur :  5.7.26-0ubuntu0.18.04.1
-- Version de PHP :  7.2.17-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `Eval2`
--

-- --------------------------------------------------------

--
-- Structure de la table `Fiche`
--

CREATE TABLE `Fiche` (
  `ID` int(11) NOT NULL,
  `Nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Espece` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Famille` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Statut` varchar(255) CHARACTER SET utf8 NOT NULL,
  `Nombre` int(11) NOT NULL,
  `Description` varchar(512) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Fiche`
--

INSERT INTO `Fiche` (`ID`, `Nom`, `Espece`, `Famille`, `Statut`, `Nombre`, `Description`) VALUES
(67, 'Fourmi Rouge', 'Fourmi', 'Formicidae', 'Sauvage', 342122, 'On appelle couramment fourmi rouge diverses espèces de fourmis de couleur rouge. Dans les régions francophones d’Europe, il s’agit le plus souvent de :      Myrmica rubra, ou fourmi rouge d’Europe. Elle peuple les milieux humides et frais d’Europe et de certaines régions d’Asie et d’Amérique du Nord.'),
(71, 'Punaise Verte', 'Hétéroptères', 'Pentatomidae', 'Sauvage', 32245653, 'Adulte elle mesure de 12 à 14 mm'),
(72, 'Taupe d\'europe', 'Taupe', 'Talpidés', 'Sauvage', 345532, 'La taupe d\'Europe est un animal de 15 à 20 cm de long pour une masse de 60 à 140 g. Son corps est cylindrique.'),
(74, 'Moustique', 'Diptères', 'Culicidés', 'Sauvage', 33452, 'Les moustiques ont un rôle dans les écosystèmes mais avant tout en épidémiologie humaine et animale'),
(76, 'Sangsue', 'Hirudinea', 'Annélides', 'Sauvage', 32344, 'Quelques espèces font l\'objet d\'un usage médicinal depuis plus de 2 000 ans');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `Fiche`
--
ALTER TABLE `Fiche`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `Fiche`
--
ALTER TABLE `Fiche`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
